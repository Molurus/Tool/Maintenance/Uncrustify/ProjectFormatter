__author__    = "Viktor Kireev"
__copyright__ = "Copyright 2018, Viktor Kireev"
__license__   = "MIT"

import os
import re
import math
from pathlib import Path
from FileFilter import FileFilter

class DirectoryScanner(object):
    def __init__(self):
        self.__root_path = Path()
        self.__mask_list = []
        self.__exclude_mask_list = []
        self.__file_list = []
        self.__reset_state()

    def __reset_state(self):
        self.__file_list = []

    def __remove_item_by_pattern(self, dir_item_list, pattern):
        fixed_list = []
        for item in dir_item_list :
            if not pattern.match(item) : fixed_list.append(item)
        return fixed_list

    def __remove_item_by_pattern_list(self, dir_item_list, pattern_list):
        for item in pattern_list :
            pattern = re.compile(item)
            dir_item_list = self.__remove_item_by_pattern(
                                dir_item_list, pattern)
        return dir_item_list

    def __remove_excluded(self, dir_item_list):
        fixed_list = self.__remove_item_by_pattern_list(
                        dir_item_list, self.__exclude_mask_list)
        return fixed_list

    def __extract_files(self, dir_item_list):
        self.__file_list.extend(FileFilter.by_mask_list(dir_item_list,
                                                               self.__mask_list))

    def __join_path_parts(self, common_path, part_list):
        path_list = []
        for part in part_list:
            path_list.append(common_path / part)
        return path_list

    def __scan_directory_level(self, current_dir_item,
                              current_level, level_count):
        if not current_dir_item.is_dir() :
            return
        dir_item_list = os.listdir(current_dir_item)
        dir_item_list = self.__remove_excluded(dir_item_list)
        dir_list = self.__join_path_parts(current_dir_item, dir_item_list)
        self.__extract_files(dir_list)
        current_level += 1
        if current_level < level_count :
            for dir_item in dir_list :
                self.__scan_directory_level(dir_item,
                                            current_level, level_count)

    def scan(self, root_path, mask_list,
             exclude_mask_list = [],
             level_count = math.inf):
        self.__root_path = root_path
        self.__mask_list = mask_list
        self.__exclude_mask_list = exclude_mask_list
        self.__reset_state()
        self.__scan_directory_level(self.__root_path, 0, level_count)

    def file_list(self):
        return self.__file_list
