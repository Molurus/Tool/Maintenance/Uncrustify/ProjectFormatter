#! /usr/bin/env python3
# -*- coding: utf-8 -*-

__author__    = "Viktor Kireev"
__copyright__ = "Copyright 2018, Viktor Kireev"
__license__   = "MIT"

import os
from pathlib import Path
import sys
import argparse
from FileFilter import FileFilter
from DirectoryScanner import DirectoryScanner
from ConfigDetector import ProjectConfigDetector, SingleConfigDetector
from Formatter import Formatter

def parse_arguments():
    parser = argparse.ArgumentParser(
        formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("project_root_path",
                        help = "project root path to format")
    parser.add_argument("--exe",
                        help = "uncrustify executable",
                        default = "uncrustify", type = str)
    parser.add_argument("-c", "--config", nargs = "?",
                        help = "use uncrustify config file CONFIG",
                        const = "uncrustify.cfg", type = str)
    parser.add_argument("--check",
                        help = "verify that source files not changed",
                        action = "store_true")
    parser.add_argument("--source_mask", nargs = "+",
                        help = "list of mask of source files to proceed",
                        default = ["\.h$", "\.hpp$", "\.cpp$"])
    parser.add_argument("--exclude_mask", nargs = "+",
                        help = "list of mask of directories to exclude",
                        default = ["^\.", "3rdparty"])
    parser.add_argument("--project_mask", nargs = "+",
                        help = "list of mask of project directories",
                        default = ["project$"])
    parser.add_argument("--config_mask", nargs = "+",
                        help = "list of mask of uncrustify config files",
                        default = ["uncrustify.cfg$"])
    parser.add_argument("--format_options", nargs = "+",
                        help = "list of options for format mode",
                        default = ["--replace", "--no-backup", "-l", "CPP", "-q", "-L 1-2"])
    parser.add_argument("--check_options", nargs = "+",
                        help = "list of options for check mode",
                        default = ["--check", "-l", "CPP", "-q", "-L 1-2"])
    parser.add_argument("-d", "--detect",
                        help = "detect uncrustify config files",
                        action = "store_true")
    parser.add_argument("--verbose",
                        help = "increase output verbosity",
                        action = "store_true")

    args = parser.parse_args()

    return args

def main(argv):
    args = parse_arguments()

    project_root_path = Path(os.path.abspath(args.project_root_path))

    print("Scan files in '%s'..." % (project_root_path))
    common_mask = args.project_mask + args.config_mask + args.source_mask
    directory_scanner = DirectoryScanner()
    directory_scanner.scan(project_root_path, common_mask, args.exclude_mask)

    common_file_list = directory_scanner.file_list()
    source_file_list = FileFilter.by_mask_list(common_file_list, args.source_mask)

    if args.config is None:
        config_detector = ProjectConfigDetector()
        config_detector.parse(common_file_list, args.project_mask, args.config_mask)
    else:
        config_detector = SingleConfigDetector(args.config)

    formatter = Formatter(args.exe, args.verbose)
    if args.check :
        print("Check stability of source files in '%s'..." % (project_root_path))
        errors_count = formatter.format(config_detector, source_file_list, args.check_options)
        if errors_count == 0:
            print("All files are stable.")
        else:
            print("Unstable file count:", errors_count)
            print("Unstable file count:", errors_count, file = sys.stderr)
    else:
        print("Format source files in '%s'..." % (project_root_path))
        errors_count = formatter.format(config_detector, source_file_list, args.format_options)
        if errors_count == 0:
            print("All files are formatted.")
        else:
            print("Format error count:", errors_count)
            print("Format error count:", errors_count, file = sys.stderr)

    print("Maintenance in '%s' completed." % (project_root_path))

    return not (errors_count == 0)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
