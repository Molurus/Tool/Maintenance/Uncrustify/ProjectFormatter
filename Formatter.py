__author__    = "Viktor Kireev"
__copyright__ = "Copyright 2018, Viktor Kireev"
__license__   = "MIT"

from pathlib import Path
import subprocess
import sys

class Formatter(object):
    def __init__(self, uncrustify_exe_path, is_verbose = False):
        self.__uncrustify_exe_path = Path(uncrustify_exe_path)
        self.__is_verbose = is_verbose
        self.__command_error_count = 0
        self.__basic_options = []

    def __define_config_for(self, file):
        # TODO: Optimize config file detection.
        return self.__config_detector.detect_config_for_source_file(file)

    def __form_command_for(self, file):
        config_file = self.__define_config_for(file)
        if config_file == Path():
            print("Uncrustify config not found for:", file, file = sys.stderr)
            return []
        uncrustify_exe = str(self.__uncrustify_exe_path)
        config_file_option = ["-c"] + [str(config_file)]
        in_file_option = [str(file)]
        command = [uncrustify_exe] \
                + self.__basic_options \
                + config_file_option \
                + in_file_option
        return command

    def __format_file(self, file):
        command = self.__form_command_for(file)
        if len(command) > 0:
            if self.__is_verbose:
                print(command)
            if subprocess.run(command).returncode != 0:
                self.__command_error_count += 1

    def __format_file_list(self, file_list):
        for file in file_list :
            self.__format_file(file)

    def format(self, config_detector, source_file_list, options):
        self.__config_detector = config_detector
        self.__basic_options = options
        self.__format_file_list(source_file_list)
        return self.__command_error_count
