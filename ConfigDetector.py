__author__    = "Viktor Kireev"
__copyright__ = "Copyright 2018, Viktor Kireev"
__license__   = "MIT"

import os
from pathlib import Path
import sys
from FileFilter import FileFilter

class ProjectConfigDetector(object):
    def __init__(self):
        self.__project_mask_list = []
        self.__config_mask_list = []
        self.__project_file_list = []
        self.__project_config_map = {}

    def __construct_project_file_list(self, file_list):
        self.__project_file_list = FileFilter.by_mask_list(file_list, self.__project_mask_list)
        for i, file in enumerate(self.__project_file_list) :
            self.__project_file_list[i] = os.path.dirname(file)

    def __construct_project_config_map(self, file_list):
        config_file_list = FileFilter.by_mask_list(file_list, self.__config_mask_list)
        self.__project_config_map = {}
        for config_file in config_file_list:
            parent_project = self.detect_parent_project(config_file)
            if parent_project != Path():
                self.__project_config_map[parent_project] = config_file
            else:
                print("Config file out of project:", config_file, file = sys.stderr)

    def parse(self, file_list, project_mask_list, config_mask_list):
        self.__project_mask_list = project_mask_list
        self.__config_mask_list = config_mask_list
        self.__construct_project_file_list(file_list)
        self.__construct_project_config_map(file_list)

    def detect_parent_project(self, file):
        for project_file in reversed(self.__project_file_list):
            if project_file != file:
                common_path = os.path.commonpath([file, project_file])
                if common_path == project_file:
                    return project_file
        return Path()

    def detect_config_for_source_file(self, source_file):
        parent_project = self.detect_parent_project(source_file)
        if parent_project != Path():
            while parent_project != Path():
                if parent_project in self.__project_config_map:
                    return self.__project_config_map[parent_project]
                else:
                    parent_project = self.detect_parent_project(parent_project)
        else:
            print("Source file out of project:", source_file, file = sys.stderr)
        return Path()

class SingleConfigDetector(object):
    def __init__(self, config_file):
        self.__config_file = config_file

    def detect_config_for_source_file(self, source_file):
        return self.__config_file
