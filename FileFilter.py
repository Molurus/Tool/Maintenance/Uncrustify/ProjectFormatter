__author__    = "Viktor Kireev"
__copyright__ = "Copyright 2018, Viktor Kireev"
__license__   = "MIT"

import re

class FileFilter(object):
    @staticmethod
    def by_mask(file_list, mask):
        filtered_file_list = []
        pattern = re.compile(mask)
        for file in file_list :
            if pattern.search(str(file)) :
                filtered_file_list.append(file)
        return filtered_file_list

    @staticmethod
    def by_mask_list(file_list, mask_list):
        filtered_file_list = []
        for mask in mask_list :
            filtered_file_list.extend(
                FileFilter.by_mask(file_list, mask))
        return filtered_file_list
